﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryMethod
{
    public class PizzeriaEcuatoriana : Pizzeria
    {

        //se crea una nueva pizza para Ecuador
        public override Pizza CrearPizza(string tipo)
        {

            //Se le implementa la etiqueta dependiento el tipo de condimento que se utilizó para su elaboración

            if (tipo == "tradicional")
            {
                return new PizzaTradicional("Ecuatoriano");
            }
            else if (tipo == "napo")
            {
                return new PizzaNapolitana("Ecuatoriano");
            }
            else
            {
                return null;
            }
        }
    }
}