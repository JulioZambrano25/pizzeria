﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryMethod
{
    public class PizzaTradicional : Pizza
    {
        public PizzaTradicional(string origen)
        {
            //aqui se describe el origen de la pizza y se le añade una descripcion

            _descripcion = "Pizza Tradicional";
            _origen = origen;
        }
    }
}