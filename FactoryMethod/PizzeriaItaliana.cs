﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryMethod
{
    public class PizzeriaItaliana : Pizzeria
    {
        // se crea una pizza para Italia
        public override Pizza CrearPizza(string tipo)
        {
            //Se le implementa la etiqueta dependiento el tipo de condimento que se utilizó para su elaboración

            if (tipo == "tradicional")
            {
                return new PizzaTradicional("Italiano");
            }
            else if (tipo == "napo")
            {
                return new PizzaNapolitana("Italiano");
            }
            else
            {
                return null;
            }
        }
    }
}