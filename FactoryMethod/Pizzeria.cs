﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryMethod
{
    
        public abstract class Pizzeria
        {
            // se crea el metodo crear pizza y se lo define de tipo string 
            public abstract Pizza CrearPizza(string tipo);
        }
    }