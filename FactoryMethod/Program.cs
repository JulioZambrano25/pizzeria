﻿using System;

namespace FactoryMethod
{
    class Program
        {
            static void Main(string[] args)
            {
                // observamos nuestras dos clases abstractas 
                Pizzeria pizzeria;
                Pizza pizza;

                // se pregunta a la pizzeria que tipos de pizza va a generar
                pizzeria = new PizzeriaEcuatoriana();
                
                // se crea la pizza y se le da un condimento distinto
                pizza = pizzeria.CrearPizza("napo");
                pizza.Render();
                pizza = pizzeria.CrearPizza("tradicional");
                pizza.Render();

                // se crea la pizza y se le da un condimento distinto
                pizzeria = new PizzeriaItaliana();
                pizza = pizzeria.CrearPizza("napo");
                pizza.Render();
                pizza = pizzeria.CrearPizza("tradicional");
                pizza.Render();
                Console.ReadKey();
            }
        }
    }
