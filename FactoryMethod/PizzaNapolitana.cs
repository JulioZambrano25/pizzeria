﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryMethod
{
    public class PizzaNapolitana : Pizza
    {
        public PizzaNapolitana(string origen)
        {
            //aqui se describe el origen de la pizza y se le añade una descripcion

            _descripcion = "Pizza napolitana";
            _origen = origen;
        }
    }
}