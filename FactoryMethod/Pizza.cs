﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryMethod
{
    public abstract class Pizza
    {
        // Cada pizza posee atributos como descripción y origen los cuales serán usado para separalos y enviarlos al lugar que corresponden.

        protected string _descripcion;
        protected string _origen;


        //Se crea un método que muestr por pantalla en forma de etiquetas las caracteristicas de la pizza
        public void Render()
        {
            Console.WriteLine(string.Format("Pizza de {0} condimentada al estilo {1}", _descripcion, _origen));


        }
    }
}